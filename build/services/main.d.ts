import { Service, Context } from "moleculer";
export default class PrunerService extends Service {
    constructor(broker: any);
    prune(ctx: Context): Promise<{
        noteCount: number;
        userCount: number;
        origNoteCount: number;
        origUserCount: number;
    }>;
}
