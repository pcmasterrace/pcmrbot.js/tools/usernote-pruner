"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const moleculer_1 = require("moleculer");
const atob = require("atob");
const btoa = require("btoa");
const pako = require("pako");
class PrunerService extends moleculer_1.Service {
    constructor(broker) {
        super(broker);
        this.parseServiceSchema({
            name: "usernotePruner",
            version: 3,
            dependencies: [
                { name: "reddit.wiki", version: 3 }
            ],
            settings: {
                subreddit: "pcmasterrace"
            },
            actions: {
                prune: {
                    name: "prune",
                    params: {
                        before: {
                            type: "date",
                            convert: true
                        }
                    },
                    handler: this.prune
                }
            }
        });
    }
    async prune(ctx) {
        const rawPage = await this.broker.call("v3.reddit.wiki.getPage", {
            subreddit: this.settings.subreddit,
            page: "usernotes"
        });
        const wrappedNotes = JSON.parse(rawPage.content_md);
        let notes = JSON.parse(pako.inflate(atob(wrappedNotes.blob), { to: "string" }));
        // Calculate original note count for comparison
        let origNoteCount = 0;
        const origUserCount = Object.keys(notes).length;
        for (let user of Object.keys(notes)) {
            let usernotes = notes[user].ns;
            origNoteCount += usernotes.length;
        }
        // Delete entries older than specified
        for (let user of Object.keys(notes)) {
            let usernotes = notes[user].ns;
            for (let i = usernotes.length - 1; i >= 0; i--) {
                if (typeof ctx.params.before === "object") {
                    if (usernotes[i].t < ctx.params.before.valueOf() / 1000) {
                        usernotes.splice(i, 1);
                    }
                }
                else if (typeof ctx.params.before === "number") {
                    if (usernotes[i].t < ctx.params.before / 1000) {
                        usernotes.splice(i, 1);
                    }
                }
                else {
                    throw new moleculer_1.Errors.MoleculerError("Date somehow isn't a number or date object");
                }
            }
            if (usernotes.length === 0) {
                delete notes[user];
            }
            else {
                notes[user].ns = usernotes;
            }
        }
        // Count remaining notes for comparison
        let noteCount = 0;
        const userCount = Object.keys(notes).length;
        for (let user of Object.keys(notes)) {
            let usernotes = notes[user];
            noteCount += usernotes.ns.length;
        }
        // Recompress for the wiki
        const deflatedBlob = btoa(pako.deflate(JSON.stringify(notes), { to: "string" }));
        wrappedNotes.blob = deflatedBlob;
        await this.broker.call("v3.reddit.wiki.setPage", {
            subreddit: this.settings.subreddit,
            page: "usernotes",
            text: JSON.stringify(wrappedNotes),
            reason: "Pruned usernotes"
        });
        return {
            noteCount,
            userCount,
            origNoteCount,
            origUserCount
        };
    }
}
exports.default = PrunerService;
