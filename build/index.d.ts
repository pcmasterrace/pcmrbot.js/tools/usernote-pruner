import { ServiceBroker } from "moleculer";
import PrunerService from "./services/main";
/**
 * Automatically loads all available services in this package
 * @param {ServiceBroker} broker
 */
export default function registerAllUsernotePrunerServices(broker: ServiceBroker): void;
export { PrunerService };
