'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
const main_1 = require('./services/main');
exports.PrunerService = main_1.default;
/**
 * Automatically loads all available services in this package
 * @param {ServiceBroker} broker
 */
function registerAllUsernotePrunerServices(broker) {
    broker.createService(main_1.default);
}
exports.default = registerAllUsernotePrunerServices;